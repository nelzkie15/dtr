-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 10:19 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendance_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(200) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`, `firstname`, `middlename`, `lastname`) VALUES
(2, 'admin', 'a1Bz20ydqelm8m1wql7df6e58ac03cb212b0d5c2047523a6cfb966bb7f', 'junil', 'a', 'toledo');

-- --------------------------------------------------------

--
-- Table structure for table `emp_message`
--

CREATE TABLE `emp_message` (
  `empID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `message` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_message`
--

INSERT INTO `emp_message` (`empID`, `name`, `email`, `message`) VALUES
(3, 'nice', 'jonelwalangkontra@yahoo.com', 'good');

-- --------------------------------------------------------

--
-- Table structure for table `late`
--

CREATE TABLE `late` (
  `late_id` int(11) NOT NULL,
  `user_no` varchar(200) NOT NULL,
  `password` varchar(40) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `mname` varchar(200) NOT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `datehire` date NOT NULL,
  `designation` text NOT NULL,
  `Department` text NOT NULL,
  `id` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `late`
--

INSERT INTO `late` (`late_id`, `user_no`, `password`, `fname`, `mname`, `lname`, `datehire`, `designation`, `Department`, `id`) VALUES
(12, 'CTcf475', 'toledo12345', 'junil', 'a', 'toledo', '0000-00-00', '', '', 'OFFLINE'),
(13, 'CT4292d', 'cecil', 'maria cecilia', 'r', 'obal', '0000-00-00', '', '', 'OFFLINE'),
(14, 'CTa48b4', '12345678', 'jonalyn', 'r', 'areglo', '0000-00-00', '', '', 'OFFLINE'),
(15, 'CT2887d', 'dar123', 'darwin', 'r', 'alborte', '0000-00-00', '', '', 'OFFLINE'),
(16, 'CT793f5', '12345678', 'jonalyn', 'r', 'areglo', '2019-04-24', 'it staff', 'Purchasing Dept', 'OFFLINE');

-- --------------------------------------------------------

--
-- Table structure for table `timein`
--

CREATE TABLE `timein` (
  `id` int(20) NOT NULL,
  `user_no` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(40) NOT NULL,
  `time` varchar(200) NOT NULL,
  `out` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timein`
--

INSERT INTO `timein` (`id`, `user_no`, `user_name`, `password`, `time`, `out`, `date`) VALUES
(3, 'CT4292d', 'Cecil', 'maria cecilia r obal', '11:02 PM', '03:48 PM', 'Aug-24-2018'),
(6, 'CT4292d', 'Cecil', 'maria cecilia r obal', '11:43 PM', '03:48 PM', 'Aug-24-2018'),
(7, 'CTcf475', 'Toledo12345', 'junil a toledo', '11:45 PM', '03:39 PM', 'Aug-24-2018'),
(8, 'CTa48b4', '12345678', 'jonalyn r areglo', '12:27 AM', '01:36 AM', 'Aug-25-2018'),
(10, 'CTa48b4', '12345678', 'jonalyn r areglo', '12:32 AM', '01:36 AM', 'Aug-25-2018'),
(11, 'CT4292d', 'Cecil', 'maria cecilia r obal', '01:29 AM', '03:48 PM', 'Aug-25-2018'),
(12, 'CTa48b4', '12345678', 'jonalyn r areglo', '01:35 AM', '01:36 AM', 'Aug-25-2018'),
(13, 'CTcf475', 'Toledo12345', 'junil a toledo', '01:37 AM', '03:39 PM', 'Aug-25-2018'),
(14, 'CTcf475', 'toledo12345', 'junil a toledo', '03:31 PM', '03:39 PM', 'Apr-24-2019'),
(15, 'CT793f5', '12345678', 'jonalyn r areglo', '03:59 PM', '04:08 PM', 'Apr-24-2019'),
(16, 'CT793f5', '12345678', 'jonalyn r areglo', '04:01 PM', '04:08 PM', 'Apr-24-2019'),
(17, 'CT793f5', '12345678', 'jonalyn r areglo', '04:06 PM', '04:08 PM', 'Apr-24-2019'),
(18, 'CTcf475', 'toledo12345', 'junil a toledo', '01:30 PM', '03:39 PM', 'Apr-25-2019'),
(19, 'CTcf475', 'toledo12345', 'junil a toledo', '01:37 PM', '03:39 PM', 'Apr-25-2019'),
(20, 'CTcf475', 'toledo12345', 'junil a toledo', '01:39 PM', '03:39 PM', 'Apr-25-2019'),
(21, 'CTcf475', 'toledo12345', 'junil a toledo', '01:40 PM', '03:39 PM', 'Apr-25-2019'),
(22, 'CTcf475', 'toledo12345', 'junil a toledo', '01:47 PM', '03:39 PM', 'Apr-25-2019'),
(23, 'CTcf475', 'toledo12345', 'junil a toledo', '01:48 PM', '03:39 PM', 'Apr-25-2019'),
(24, 'CTcf475', 'toledo12345', 'junil a toledo', '01:50 PM', '03:39 PM', 'Apr-25-2019'),
(25, 'CTcf475', 'toledo12345', 'junil a toledo', '01:51 PM', '03:39 PM', 'Apr-25-2019'),
(26, 'CTcf475', 'toledo12345', 'junil a toledo', '01:52 PM', '03:39 PM', 'Apr-25-2019'),
(27, 'CTcf475', 'toledo12345', 'junil a toledo', '01:53 PM', '03:39 PM', 'Apr-25-2019'),
(28, 'CTcf475', 'toledo12345', 'junil a toledo', '01:55 PM', '03:39 PM', 'Apr-25-2019'),
(29, 'CTcf475', 'toledo12345', 'junil a toledo', '01:58 PM', '03:39 PM', 'Apr-25-2019'),
(30, 'CTcf475', 'toledo12345', 'junil a toledo', '02:00 PM', '03:39 PM', 'Apr-25-2019'),
(31, 'CTcf475', 'toledo12345', 'junil a toledo', '02:04 PM', '03:39 PM', 'Apr-25-2019'),
(32, 'CTcf475', 'toledo12345', 'junil a toledo', '02:05 PM', '03:39 PM', 'Apr-25-2019'),
(33, 'CTcf475', 'toledo12345', 'junil a toledo', '02:08 PM', '03:39 PM', 'Apr-25-2019'),
(34, 'CTcf475', 'toledo12345', 'junil a toledo', '02:09 PM', '03:39 PM', 'Apr-25-2019'),
(35, 'CTcf475', 'toledo12345', 'junil a toledo', '02:13 PM', '03:39 PM', 'Apr-25-2019'),
(36, 'CTcf475', 'toledo12345', 'junil a toledo', '03:11 PM', '03:39 PM', 'Apr-25-2019'),
(37, 'CTcf475', 'toledo12345', 'junil a toledo', '03:14 PM', '03:39 PM', 'Apr-25-2019'),
(38, 'CTcf475', 'toledo12345', 'junil a toledo', '03:17 PM', '03:39 PM', 'Apr-25-2019'),
(39, 'CTcf475', 'toledo12345', 'junil a toledo', '03:19 PM', '03:39 PM', 'Apr-25-2019'),
(40, 'CTcf475', 'toledo12345', 'junil a toledo', '03:19 PM', '03:39 PM', 'Apr-25-2019'),
(41, 'CTcf475', 'toledo12345', 'junil a toledo', '03:19 PM', '03:39 PM', 'Apr-25-2019'),
(42, 'CTcf475', 'toledo12345', 'junil a toledo', '03:20 PM', '03:39 PM', 'Apr-25-2019'),
(43, 'CTcf475', 'toledo12345', 'junil a toledo', '03:20 PM', '03:39 PM', 'Apr-25-2019'),
(44, 'CTcf475', 'toledo12345', 'junil a toledo', '03:22 PM', '03:39 PM', 'Apr-25-2019'),
(45, 'CTcf475', 'toledo12345', 'junil a toledo', '03:22 PM', '03:39 PM', 'Apr-25-2019'),
(46, 'CTcf475', 'toledo12345', 'junil a toledo', '03:23 PM', '03:39 PM', 'Apr-25-2019'),
(47, 'CTcf475', 'toledo12345', 'junil a toledo', '03:25 PM', '03:39 PM', 'Apr-25-2019'),
(48, 'CTcf475', 'toledo12345', 'junil a toledo', '03:27 PM', '03:39 PM', 'Apr-25-2019'),
(49, 'CTcf475', 'toledo12345', 'junil a toledo', '03:27 PM', '03:39 PM', 'Apr-25-2019'),
(50, 'CTcf475', 'toledo12345', 'junil a toledo', '03:27 PM', '03:39 PM', 'Apr-25-2019'),
(51, 'CTcf475', 'toledo12345', 'junil a toledo', '03:28 PM', '03:39 PM', 'Apr-25-2019'),
(52, 'CTcf475', 'toledo12345', 'junil a toledo', '03:29 PM', '03:39 PM', 'Apr-25-2019'),
(53, 'CTcf475', 'toledo12345', 'junil a toledo', '03:33 PM', '03:39 PM', 'Apr-25-2019'),
(54, 'CTcf475', 'toledo12345', 'junil a toledo', '03:36 PM', '03:39 PM', 'Apr-25-2019'),
(55, 'CT4292d', 'cecil', 'maria cecilia r obal', '03:47 PM', '03:48 PM', 'Apr-25-2019'),
(56, 'CT2887d', 'dar123', 'darwin r alborte', '03:52 PM', '03:52 PM', 'Apr-25-2019');

-- --------------------------------------------------------

--
-- Table structure for table `timeout`
--

CREATE TABLE `timeout` (
  `id` int(20) NOT NULL,
  `user_no` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `time` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `emp_message`
--
ALTER TABLE `emp_message`
  ADD PRIMARY KEY (`empID`);

--
-- Indexes for table `late`
--
ALTER TABLE `late`
  ADD PRIMARY KEY (`late_id`);

--
-- Indexes for table `timein`
--
ALTER TABLE `timein`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeout`
--
ALTER TABLE `timeout`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `emp_message`
--
ALTER TABLE `emp_message`
  MODIFY `empID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `late`
--
ALTER TABLE `late`
  MODIFY `late_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `timein`
--
ALTER TABLE `timein`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `timeout`
--
ALTER TABLE `timeout`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
