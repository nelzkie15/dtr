<!DOCTYPE html>
<?php

session_start();
if(!isset($_SESSION["username"])){
    header("index.php");

}
?>

<html lang = "eng">
	<head>
		<title>Time Keeper | Home</title>
		<meta charset = "utf-8" />
		<meta name = "viewport" content = "width=device-width, initial-scale=1" />
	    <meta http-equiv="refresh" content="5">
		<link rel = "stylesheet" href = "css/bootstrap.css" />
		<link rel = "stylesheet" href = "css/jquery.dataTables.css" />



	</head>
	<body>
		<nav class = "navbar navbar-inverse navbar-fixed-top">
			<div class = "container-fluid">
				<div class = "navbar-header">
				<p class = ""><?php include('animate/index.html');?></p>
				</div>
				<ul class = "nav navbar-nav navbar-right">
					<li class = "dropdown">
							<?php 
    include 'connect.php';

   $id = $_SESSION['username'];


	$r = mysqli_query($conn,"SELECT * FROM admin where admin_id = '$id'") or die (mysqli_error($con));

	$row = mysqli_fetch_array($r);

	 $id=$row['username'];
	 $lname=$row['lastname'];

	 



?>
						<a href = "#" class = "dropdown-toggle" data-toggle = "dropdown"><i class = "glyphicon glyphicon-user"></i> <?php echo htmlentities($id.' '.$lname)?> <b class = "caret"></b></a>
						<ul class = "dropdown-menu">
							<li><a href = "logout.php"><i class = "glyphicon glyphicon-off"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<div class = "container-fluid" style = "margin-top:70px;">
			<ul class = "nav nav-pills">
				<li><a href = "home.php"><span class = "glyphicon glyphicon-home"></span> Home</a></li>

				<li class = "dropdown">
					<a class = "dropdown-toggle" data-toggle = "dropdown" href = "#"><span class = "glyphicon glyphicon-book"></span> Records <span class = "caret"></span></a>
					<ul class = "dropdown-menu">
					
						<li><a href = "view_attendance.php"><span class = "glyphicon glyphicon-time"></span>Time Record</a></li>
					</ul>
				</li>
			
			</ul>
			<br />
			<div class = "alert alert-info">Home/ Employee / <a href="admin.php"style="float:right;">Go To Admin Page</a></div>
			<div class = "modal fade" id = "add_student" tabindex = "-1" role = "dialog" aria-labelledby = "myModallabel">
				<div class = "modal-dialog" role = "document">
					<div class = "modal-content panel-primary">
						<div class = "modal-header panel-heading">
							<button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
							<span aria-hidden = "true">&times;</span></button>
							<h4 class = "modal-title" id = "myModallabel">Add New Employee</h4>
						</div>
						<form method = "POST" action = "save_emp_query.php" enctype = "multipart/form-data">
							<div class  = "modal-body">
								<div class = "form-group">
									<label>Employee ID:</label>
									<input type = "text"  name = "user_no" value="CT<?php $prefix= sha1(time()*rand(1, 2)); echo strip_tags(substr($prefix ,0,5));?>" required = "required" maxlength="14" minlength="3" class = "form-control"  disabled="disabled"/>
										<input type = "hidden"  name = "user_no" value="CT<?php $prefix= sha1(time()*rand(1, 2)); echo strip_tags(substr($prefix ,0,5));?>" required = "required" maxlength="14" minlength="3" class = "form-control" />
								</div>
									<div class = "form-group">
									<label>Password:</label>
									<input type = "password"  name = "password" required = "required" maxlength="15" minlength="3" class = "form-control" />
								</div>
								<div class = "form-group">
									<label>Firstname:</label>
									<input type = "text" style=" text-transform:capitalize;" name = "firstname" required = "required" class = "form-control" />
								</div>
								<div class = "form-group">
									<label>Middlename:</label>
									<input type = "text" style=" text-transform:capitalize;" name = "middlename" placeholder = "(Optional)" class = "form-control" />
								</div>
								<div class = "form-group">
									<label>Lastname:</label>
									<input type = "text" style=" text-transform:capitalize;" name = "lastname" required = "required" class = "form-control" />
								</div>
								
							</div>
							<div class = "modal-footer">
								<button  class = "btn btn-primary" name = "save"><span class = "glyphicon glyphicon-save"></span> Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class = "modal fade" id = "delete" tabindex = "-1" role = "dialog" aria-labelledby = "myModallabel">
				<div class = "modal-dialog" role = "document">
					<div class = "modal-content ">
						<div class = "modal-body">
							<center><label class = "text-danger">Are you sure you want to delete this record?</label></center>
							<br />
							<center><a class = "btn btn-danger remove_id" >
							<span class = "glyphicon glyphicon-trash"></span> Yes</a> 
							<button type = "button" class = "btn btn-warning" data-dismiss = "modal" aria-label = "No">
							<span class = "glyphicon glyphicon-remove"></span> No</button></center>
						</div>
					</div>
				</div>
			</div>
			<div class = "modal fade" id = "edit_student" 
			tabindex = "-1" role = "dialog" aria-labelledby = "myModallabel">
				<div class = "modal-dialog" role = "document">
					<div class = "modal-content panel-warning">
						<div class = "modal-header panel-heading">
							<button type = "button" class = "close" data-dismiss = "modal" 
							aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
							<h4 class = "modal-title" id = "myModallabel">Edit Employee</h4>
						</div>
						<div id = "edit_query">
						</div>
					</div>
				</div>
			</div>
			<div class = "well col-lg-12">
				<button class = "btn btn-success" type = "button" href = "#" data-toggle = "modal" data-target = 
				"#add_student"><span class = "glyphicon glyphicon-plus"></span> Add New </button>
				<a href="javascript:print()">
<button class="btn btn-primary"><i class = 'glyphicon glyphicon-print'></i>&nbsp;Print</button></a>
				<br />
				<br />
	<div class="content" id="content" >	
				<table id = "table" class = "table table-striped">
					<thead class = "alert-info">
						<tr>
							<th>Employee ID</th>
							<th>Password</th>
							<th>Firstname</th>
							<th>Middlename</th>
							<th>Lastname</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$q_student = $conn->query("SELECT * FROM `late`") or die(mysqli_error());
							while($f_student = $q_student->fetch_array()){
						?>
						<tr  style=" text-transform:capitalize;">
							<td><?php echo $f_student['user_no']?></td>
							<td><?php echo $f_student['password']?></td>							
							<td><?php echo $f_student['fname']?></td>
							<td><?php echo $f_student['mname']?></td>
							<td><?php echo $f_student['lname']?></td>
							<?php 
							if($f_student['id']=="ONLINE"){
								echo "<td style='background:#00ff00;color:white;font-size:120%'><i class = 'glyphicon glyphicon-user'></i>&nbsp;ONLINE</td> ";
							}else{
								echo "<td style='background:#ff0000;color:white;font-size:120%'><i class = 'glyphicon glyphicon-user'></i>&nbsp;OFFLINE</td> ";
							}
							
							?>
								
							<td>
						
							<a class = "btn btn-danger rstudent_id" onclick="return confirm('Are You Sure?')"
							href = "student.php?idx=<?php echo $f_student['late_id']?>">
							<span class = "glyphicon glyphicon-remove">
							</span></a> 
							<?php
							}
							if(isset($_GET['idx'])){
								$idx=$_GET['idx'];
								$result=$conn->query("DELETE FROM late WHERE late_id='$idx'");
								if($result){
									?>
									<script>
									alert('Successful Deleted');
									window.location.href='student.php';
									</script>
									<?php
								}else{
									?>
									<script>
									alert('Fail To Delete The Student');
									window.location.href='student.php';
									</script>
									<?php
								}
							}
							?>
						</td>
						
						</tr>
						<?php
							
						?>
					</tbody>
				</table>
				</div>
			</div>
			<br />
			<br />	
			<br />	
		</div>
	
			
		</div>	
	</body>
	<script src = "js/jquery.js"></script>
	<script src = "js/bootstrap.js"></script>
	<script src = "js/jquery.dataTables.js"></script>
	<script type = "text/javascript">
		$(document).ready(function(){
			$('#table').DataTable();
		});
	</script>
	<script type = "text/javascript">
		$(document).ready(function(){
			$('.rstudent_id').click(function(){
				$student_id = $(this).attr('name');
				$('.remove_id').click(function(){
					window.location = 'delete_student.php?student_id=' + $student_id;
				});
			});
			$('.estudent_id').click(function(){
				$student_id = $(this).attr('name');
				$('#edit_query').load('load_edit1.php?student_id=' + $student_id);
			});
		});
	</script>
	<script language="javascript">


function print()
{ 
  var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
      disp_setting+="scrollbars=yes,"; 
  var content_vlue = document.getElementById("content").innerHTML; 
  var docprint=window.open("","",disp_setting); 
   docprint.document.open(); 
   docprint.document.write('<title>Employee Members</title>');   
   docprint.document.write('<link href="css/style_contentko.css" rel="stylesheet" type="text/css" media="print"/>'); 
     
   docprint.document.write('<body onLoad="self.print()" style=" font-size: 5px; font-family: arial;">'); 
   docprint.document.write('<hr>');        
   docprint.document.write(content_vlue); 

   docprint.document.write('</body">');      
   docprint.document.close(); 
   docprint.focus(); 
}
</script>
</html>