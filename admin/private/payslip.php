<?php

session_start();
if(!isset($_SESSION["username"])){
    header("location:../index.php");

}
?>

<!DOCTYPE html>

<html lang = "eng">
  <head>
    <title>Time Keeper | Home</title>
    <meta charset = "utf-8" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1" />
     <!-- <meta http-equiv="refresh" content="5">-->
    <link rel = "stylesheet" href = "css/bootstrap.css" />
    <link rel = "stylesheet" href = "css/jquery.dataTables.css" />

    <script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript" >
        // Disen
        document.onselectstart=new Function('return false');
        function dMDown(e) {return false;}
        function dOClick() {return true;}
        document.onmousedown=dMDown;
        document.onclick=dOClick
        
        -->
    </script>
<style>
body, html
{
	margin:0;
	padding:0;
	font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-size:11px;
	background-color:#fff;
}
#outerwrapper
{
	width:800px;
	height:auto;
	margin:auto;

}

#slip
{
	width:747px;
	height:auto;
	margin:auto;
	font-size: x-small;
}

#slip2
{
	width:418px;
	height:auto;
	margin-left:150px;
	margin-top:40px;
	text-align: right;
}

.text
{
	font-size:14px;
	font-family:Tahoma, Geneva, sans-serif;
	font-weight:bold;
}
</style>
</head>

<body>

         
<div id="outerwrapper">
<div id="slip">
<table width="747" border="1">
  <br>
    <div class="pull-right">
<button type="hidden" href=""  onclick = "window.print()"><span class = "glyphicon glyphicon-print"></span></button></a> 
</div>
<br>
<hr>
  <tr>
    <br>
    <td colspan="4" align="center"><span class="text"><br>INTERNATIONAL COMPANIES LIMITED</span><br />
      <span class="text">NO 11, YOUR AREA, YOUR TOWN, YOUR CITY</span><br />
 
      <br />
<br />
     <br />
<br />
      <table width="200" border="0">
        <tr>
          <td align="center">PAYSLIP</td>
        </tr>
      </table></td>
    </tr>
 
    
      <?php 

    include 'connect.php';
          
           $q_time1 = mysqli_query($conn,"SELECT * FROM `late`") or die(mysqli_error());
           $f_time1 = mysqli_fetch_array($q_time1);

            $q_time = mysqli_query($conn,"SELECT * FROM `timein`") or die(mysqli_error());
           $f_time = mysqli_fetch_array($q_time);



              $id =   $f_time['id'];
              $user_no =   $f_time['user_no'];
              $user_name =   $f_time['user_name'];
              $password =   $f_time['password'];
            $timein =   $f_time['time'];
            $out =  $f_time['out'];
            $date =  $f_time['date'];

            $datehire =  $f_time1['datehire'];
            $designation =  $f_time1['designation'];
            $Department =  $f_time1['Department'];
           // $date1 = "hrs/min";
           // $hrs = 8;

            $datetime1 = new  DateTime($timein);
             $datetime2 = new DateTime($out);

             $interval = $datetime1->diff($datetime2);

             $nel = "Php 5,000";

          ?> 
     <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Employee ID</strong></td> 
    <td><?php echo $user_no;?></td>
    <td><strong>Time In</strong></td>
    <td><i><?php echo $timein;?></i></td>
  </tr>
  <tr>
    <td><strong>Employee Name</strong></td>
    <td><?php echo $password; ?></td>
    <td><strong>Time Out</strong></td>
    <td width="181"><?php echo $out; ?></td>
  </tr>
  <tr>
     <td><strong>Position</strong></td>
    <td><?php echo $designation; ?></td>
    
    <td width="83"><strong>Total Per Day</strong></td>
    <td><?php echo $interval->format('%h') - (1).  "&nbsp;Hours&nbsp;".$interval->format('%i')."&nbsp;Minutes";?></td>
  </tr>
  <tr>
    <td width="87"><strong>Department</strong></td>
    <td width="368"><?php echo $Department; ?></td>
    <td><strong>Over Time</strong></td>
    <td><?php echo $interval->format('%h') - (9).  "&nbsp;Hrs&nbsp;".$interval->format('%i')."&nbsp;Min";?></td>
      
  </tr>
  <tr>
  

    <td colspan="4">
    <div id="slip2">
    <table width="418" border="1">
      <tr>
        <td width="180"><strong>Basic Salary</strong></td>
        <td width="222"><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Housing Allowance</strong></td>
        <td><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Meal Allowance</strong></td>
        <td><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Transport Allowance</strong></td>
        <td><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Entertainment Allowance</strong></td>
        <td><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Long Service Allowance</strong></td>
        <td><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Tax Deductions</strong></td>
        <td><?php echo $nel; ?></td>
      </tr>
      <tr>
        <td><strong>Net Total</strong></td>
        <td><strong>N<?php echo $nel; ?></strong></td>
      </tr>
    </table>
   </div></td>
  </tr>
</table>
 
<table width="747" border="1">
  <tr>
    <td align="center"><br />
      ............................................................<br />
      Acountant </td>
    <td align="center"><br />
      ............................................................<br />
      Finance Manager</td>
  </tr>
  </table>
  <br><br>

<br />

</a>
</div>
</div>
</body>
</html>
