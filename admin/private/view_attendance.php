<?php

session_start();
if(!isset($_SESSION["username"])){
    header("location:../index.php");

}
?>


<!DOCTYPE html>

<html lang = "eng">
	<head>
		<title>Time Keeper | Home</title>
		<meta charset = "utf-8" />
		<meta name = "viewport" content = "width=device-width, initial-scale=1" />
	   <!-- <meta http-equiv="refresh" content="5">-->
		<link rel = "stylesheet" href = "css/bootstrap.css" />
		<link rel = "stylesheet" href = "css/jquery.dataTables.css" />

		<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>


	</head>
	<body>
		<nav class = "navbar navbar-inverse navbar-fixed-top">
			<div class = "container-fluid">
				<div class = "navbar-header">
				<p class = ""><?php include('animate/index.html');?></p>
				</div>
				<ul class = "nav navbar-nav navbar-right">
					<li class = "dropdown">
							<?php 
							    include 'connect.php';

							   $id = mysqli_real_escape_string($conn,$_SESSION['username']);


								$r = mysqli_query($conn,"SELECT * FROM admin where admin_id = '$id'") or die (mysqli_error($con));

								$row = mysqli_fetch_array($r);

								 $id=$row['username'];
								 $lname=$row['lastname'];

								 



							?>
						<a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">Admin, <i class = "glyphicon glyphicon-user"></i> <?php echo htmlentities($id.' '.$lname)?> <b class = "caret"></b></a>
						<ul class = "dropdown-menu">
							<li><a href = "logout.php"><i class = "glyphicon glyphicon-off"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<div class = "container-fluid" style = "margin-top:70px;">
			<ul class = "nav nav-pills">
				<li><a href = "home.php"><span class = "glyphicon glyphicon-home"></span> Home</a></li>
				<li class = "dropdown active">
					<a class = "dropdown-toggle" data-toggle = "dropdown" href = "#"><span class = "glyphicon glyphicon-book"></span> Records <span class = "caret"></span></a>
					<ul class = "dropdown-menu">
						
						<li><a href = "view_attendance.php"><span class = "glyphicon glyphicon-time"></span>Time Record</a></li>

					</ul>
				</li>
			
			</ul>
			<br />
			<div class = "alert alert-info">Attendance Record  <a href="employee.php"style="float:right;">Go To Employee Page</a></div>
			<div class = "modal fade" id = "delete" tabindex = "-1" role = "dialog" aria-labelledby = "myModallabel">
				<div class = "modal-dialog" role = "document">
					<div class = "modal-content ">
						<div class = "modal-body">
							<center><label class = "text-danger">Are you sure you want to delete this record?</label></center>
							<br />
							<center><a class = "btn btn-danger remove_id" ><span class = "glyphicon glyphicon-trash"></span> Yes</a> 
							<button type = "button" class = "btn btn-warning" data-dismiss = "modal" aria-label = "No">
							<span class = "glyphicon glyphicon-remove"></span> No</button></center>
						</div>
					</div>
				</div>
			</div>
				
	<div class = "well col-lg-12">	
	<a href="javascript:print()">
<button class="btn btn-primary"><i class = 'glyphicon glyphicon-print'></i>&nbsp;Pdf</button></a> <button id="export" class="btn btn-success"><i class = 'glyphicon glyphicon-print'></i>&nbsp;Excel</button><div class="content" id="content" >	
	<table id = "table" class = "table table-striped">
            
					<thead class = "alert-info">
						<tr>
							
							<th>Employee ID</th>
							<th>Employee Details</th>						
							<th>Time in</th>
							<th>Time out</th>
							<th>Total Per Day</th>
							<th>Overtime</th>							
							<th>Date</th>							
							<th>Action</th>
						
						</tr>
					</thead>
					<tbody>
					<?php
					
				

						$q_time = $conn->query("SELECT * FROM `timein` order by user_name desc") or die(mysqli_error());
						while($f_time=$q_time->fetch_array()){
							$id = 	$f_time['id'];
						$timein = 	$f_time['time'];
						$out = 	$f_time['out'];
					 // $date1 = "hrs/min";
					 // $hrs = 8;


                    //  $total = strtotime($timein) + strtotime($out) - $hrs;   

                     $datetime1 = new  DateTime($timein);
                     $datetime2 = new DateTime($out);

                     $interval = $datetime1->diff($datetime2);



                     /*$nel = strtotime("$timein") - strtotime("$out");

                    $Hours = abs(floor($nel/3600));

                    echo $Hours;*/

					?>	

						<tr>
							<td><?php echo $f_time['user_no']?></td>
							<td><?php echo htmlentities(ucwords($f_time['password']))?></td>
						
							
							

							<td width="15%"><?php echo $f_time['time']?></td>
							<td width="15%"><?php echo $f_time['out']?></td>
							<td><?php echo $interval->format('%h') - (1).  "&nbsp;Hours&nbsp;".$interval->format('%i')."&nbsp;Minutes";?></td>
							<td><?php echo $interval->format('%h') - (9).  "&nbsp;Hrs&nbsp;".$interval->format('%i')."&nbsp;Min";?></td>
							<td width="15%"><?php echo date("M-d-Y", htmlentities(strtotime($f_time['date'])))?></td>

							<td>
							<a class = "btn btn-danger ruser_id" onclick="return confirm('Are You Sure?')"
							href = "view_attendance.php?idx=<?php echo htmlentities($f_time['id'])?>">
							<span class = "glyphicon glyphicon-remove">
							</span></a> 
							<?php
							}
							if(isset($_GET['idx'])){
								$idx=mysqli_real_escape_string($conn,$_GET['idx']);
								$result=$conn->query("DELETE FROM timein WHERE id='$idx'");
								if($result){
									?>
									<script>
									alert('Successful Deleted');
									window.location.href='view_attendance.php';
									</script>
									<?php
								}else{
									?>
									<script>
									alert('Fail To Delete The Employee');
									window.location.href='view_attendance.php';
									</script>
									<?php
								}
							}
							?>
						</td>
						
						</tr>
						
					<?php
						
						echo"
						
						<br>
					
						"
					?>	
			
					
					</tbody>
			

		
                  
				</table>
           
			 </div>
			</div>


			
		</div>	
	</body>
	<script src = "js/jquery.js"></script>
	<script src = "js/bootstrap.js"></script>
	<script src = "js/jquery.dataTables.js"></script>
	<script src="table2excel/src/jquery.table2excel.js" type="text/javascript"></script>

<script>
    $("#export").click(function(){
        $("#table").table2excel({

            // exclude CSS class
            exclude: ".noExl",
            name: "Worksheet Name",
        filename: "EmployeeReport" //do not include extension
    });
    });

</script>
	<script type = "text/javascript">
		$(document).ready(function(){
			$('#table').DataTable();
		});
	</script>
	<script type = "text/javascript">
		$(document).ready(function(){
			$('.ruser_id').click(function(){
				$late_id = $(this).attr('name');
				$('.remove_id').click(function(){
					window.location = 'delete_emp.php?id=' + $id;
				});
			});
		});
	</script>


	<script language="javascript">

function print()
{ 
  var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
      disp_setting+="scrollbars=yes,width=800, height=700, left=500, top=10"; 
  var content_vlue = document.getElementById("content").innerHTML; 
  var docprint=window.open("","",disp_setting); 
   docprint.document.open(); 
   docprint.document.write('<title>Attendance Record</title>');   
   docprint.document.write('<link href="css/style_contentko.css" rel="stylesheet" type="text/css" media="print"/>');    
   docprint.document.write('<body onLoad="self.print()" style="width: 800px; font-size: 5px; font-family: arial">'); 
  docprint.document.write("<hr>"); 
   docprint.document.write(content_vlue); 
      
   docprint.document.write('</body">');      
   docprint.document.close(); 
   docprint.focus(); 
}
</script>
</html>

