<?php

session_start();
if(!isset($_SESSION["username"])){
    header("location:../index.php");

}
?>

<!DOCTYPE html>
<html lang = "eng">
	<head>
		<title>Time Keeper | Home</title>
		<meta charset = "utf-8" />
		<meta name = "viewport" content = "width=device-width, initial-scale=1" />

		<link rel = "stylesheet" href = "css/bootstrap.css" />
		<link rel = "stylesheet" href = "css/jquery.dataTables.css" />

		<link rel = "stylesheet" type = "text/css" href = "css/jquery-ui.css"/>



	</head>
	<body>
		<nav class = "navbar navbar-inverse navbar-fixed-top">
			<div class = "container-fluid">
				<div class = "navbar-header">
				<p class = ""><?php include('animate/index.html');?></p>
				</div>
				<ul class = "nav navbar-nav navbar-right">
					<li class = "dropdown">
							<?php 
    include 'connect.php';

   $id = mysqli_real_escape_string($conn,$_SESSION['username']);


	$r = mysqli_query($conn,"SELECT * FROM admin where admin_id = '$id'") or die (mysqli_error($con));

	$row = mysqli_fetch_array($r);

	 $id=$row['username'];
	 $lname=$row['lastname'];

	 



?>
						<a href = "#" class = "dropdown-toggle" data-toggle = "dropdown"><i class = "glyphicon glyphicon-user"></i> <?php echo htmlentities($id.' '.$lname)?> <b class = "caret"></b></a>
						<ul class = "dropdown-menu">

							<li><a href = "logout.php"><i class = "glyphicon glyphicon-off"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
			<div class = "container-fluid" style = "margin-top:70px;">
			<ul class = "nav nav-pills">
				<li><a href = "home.php"><span class = "glyphicon glyphicon-home"></span> Home</a></li>

				<li class = "dropdown">
					<a class = "dropdown-toggle" data-toggle = "dropdown" href = "#"><span class = "glyphicon glyphicon-book"></span> Payroll <span class = "caret"></span></a>
					<ul class = "dropdown-menu">
					
						<li><a href = "payslip.php"><span class = "glyphicon glyphicon-list"></span> Payslip</a></li>
					</ul>
				</li>
			
			</ul>
	
			<div class = "alert alert-info">Home/ Payroll / <a href="home.php"style="float:right;">Go To Home Page</a>
             

			</div>
			<div class = "container">
			
               <h3>Search Employee</h3>
               <hr style = "border-top:1px dotted #000;"/>
               	<div class = "form-inline">
			  <form action="" method="POST">
			  	Date:<input type="date" name="startdate" class="form-control" placeholder="startdate">&nbsp;To:<input type="date" class="form-control" name="enddate" placeholder="enddate">
			  	<input type="submit" class="btn btn-primary" name="go" value="search"> <input type = "reset" id = "reset" class = "btn btn-success" value="Reset">
			</div>
        </div>
</html>


<?php 

include 'connect.php';

if(isset($_POST['go'])){

	$startdate =  date("M-d-Y", strtotime($_POST['startdate']));
	$enddate = date("M-d-Y", strtotime($_POST['enddate']));

	$query = mysqli_query($conn,"SELECT password FROM timein where date BETWEEN '$startdate' and '$enddate' ORDER BY  date limit 0, 1");

	$counter = mysqli_num_rows($query);


if($counter == 0){

	 echo "<br>";

	     echo "<div class = 'container'><div class='alert alert-danger alert-dismissable fade in'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>No Record Found!</strong> 
  </div></div>";

}else{

	while($row = mysqli_fetch_array($query)){



	   $w = ucwords($row['password']);

	   $r = '<p>'.$w.'</p>';

	   //echo  $r;
	   echo "<br>";

	     /*echo "<div class = 'container'><div class='alert alert-success alert-dismissable fade in'>
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    <strong>$r</strong> 
  </div></div>";*/

  echo "<div class = 'container'>";
      echo "<table  class = 'table table-striped'>";
      echo "<thead class = 'alert-info'>";
			echo "<tr>";
     echo "<th>Fullname</th>";
           echo "</tr>";
           echo "<tbody>";
           echo "<tr>";
           echo "<td> $r</td>";
           echo "</tr>";
           echo "</tbody>";
     echo "</table>";

  "</div>";


       
   	}
  }

}


?>
</div>
</body>
	<script src = "js/jquery.js"></script>
	<script src = "js/bootstrap.js"></script>