<?php

session_start();
if(!isset($_SESSION["username"])){
    header("location:../index.php");

}
?>

<!DOCTYPE html>
<html lang = "eng">
	<head>
		<title>Time Keeper | Home</title>
		<meta charset = "utf-8" />
		<meta name = "viewport" content = "width=device-width, initial-scale=1" />
		<meta http-equiv="refresh" content="5">
		<link rel = "stylesheet" href = "css/bootstrap.css" />
		<link rel = "stylesheet" href = "css/jquery.dataTables.css" />



	</head>
	<body>
		<nav class = "navbar navbar-inverse navbar-fixed-top">
			<div class = "container-fluid">
				<div class = "navbar-header">
				<p class = ""><?php include('animate/index.html');?></p>
				</div>
				<ul class = "nav navbar-nav navbar-right">
					<li class = "dropdown">
							<?php 
    include 'connect.php';

   $id = mysqli_real_escape_string($conn,$_SESSION['username']);


	$r = mysqli_query($conn,"SELECT * FROM admin where admin_id = '$id'") or die (mysqli_error($con));

	$row = mysqli_fetch_array($r);

	 $id=$row['username'];
	 $lname=$row['lastname'];

	 



?>
						<a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">Admin, <i class = "glyphicon glyphicon-user"></i> <?php echo htmlentities($id.' '.$lname)?> <b class = "caret"></b></a>
						<ul class = "dropdown-menu">
							<li><a href = "logout.php"><i class = "glyphicon glyphicon-off"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<div class = "container-fluid" style = "margin-top:70px;">
			<ul class = "nav nav-pills">
				<li><a href = "home.php"><span class = "glyphicon glyphicon-home"></span> Home</a></li>

				<li class = "dropdown">
					<a class = "dropdown-toggle" data-toggle = "dropdown" href = "#"><span class = "glyphicon glyphicon-book"></span> Records <span class = "caret"></span></a>
					<ul class = "dropdown-menu">
					
						<li><a href = "view_attendance.php"><span class = "glyphicon glyphicon-time"></span>Time Record</a></li>
					</ul>
				</li>
			
			</ul>
			<br />
			<div class = "alert alert-info">Home/ Message / <a href="home.php"style="float:right;">Go To Home Page</a></div>
		
			<div class = "modal fade" id = "delete" tabindex = "-1" role = "dialog" aria-labelledby = "myModallabel">
				<div class = "modal-dialog" role = "document">
					<div class = "modal-content ">
						<div class = "modal-body">
							<center><label class = "text-danger">Are you sure you want to delete this record?</label></center>
							<br />
							<center><a class = "btn btn-danger remove_id" >
							<span class = "glyphicon glyphicon-trash"></span> Yes</a> 
							<button type = "button" class = "btn btn-warning" data-dismiss = "modal" aria-label = "No">
							<span class = "glyphicon glyphicon-remove"></span> No</button></center>
						</div>
					</div>
				</div>
			</div>
			<div class = "modal fade" id = "edit_student" 
			tabindex = "-1" role = "dialog" aria-labelledby = "myModallabel">
				<div class = "modal-dialog" role = "document">
					<div class = "modal-content panel-warning">
						<div class = "modal-header panel-heading">
							<button type = "button" class = "close" data-dismiss = "modal" 
							aria-label = "Close"><span aria-hidden = "true">&times;</span></button>
							<h4 class = "modal-title" id = "myModallabel">Edit Employee</h4>
						</div>
						<div id = "edit_query">
						</div>
					</div>
				</div>
			</div>
			<div class = "well col-lg-12">
	
				<br />
				<br />
	<div class="content" id="content" >	
				<table id = "table" class = "table table-striped">
					<thead class = "alert-info">
						<tr>
							<th>ID</th>
							<th>EmployeeID</th>
							<th>Email</th>
							<th>Messages</th>
						
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$q_emp_message = $conn->query("SELECT * FROM `emp_message`") or die(mysqli_error());
							while($f_emp_message = $q_emp_message->fetch_array()){
						?>
						<!--<tr  style=" text-transform:capitalize;">-->
							<tr>
							<td><?php echo htmlentities($f_emp_message['empID'])?></td>
							<td><?php echo htmlentities($f_emp_message['name'])?></td>							
							<td><?php echo htmlentities($f_emp_message['email'])?></td>
							<td><?php echo htmlentities(ucwords($f_emp_message['message']))?></td>
							
					
							<td>
						
							<a class = "btn btn-danger rstudent_id" onclick="return confirm('Are You Sure?')"
							href = "emp_message.php?idx=<?php echo htmlentities($f_emp_message['empID'])?>">
							<span class = "glyphicon glyphicon-remove">
							</span></a> 
							<?php
							}
							if(isset($_GET['idx'])){
								$idx=mysqli_real_escape_string($conn,$_GET['idx']);
								$result=$conn->query("DELETE FROM emp_message WHERE empID='$idx'");
								if($result){
									?>
									<script>
									alert('Successful Deleted');
									window.location.href='emp_message.php';
									</script>
									<?php
								}else{
									?>
									<script>
									alert('Fail To Delete The Messages');
									window.location.href='emp_message.php';
									</script>
									<?php
								}
							}
							?>
						</td>
						
						</tr>
						<?php
							
						?>
					</tbody>
				</table>
				</div>
			</div>
			<br />
			<br />	
			<br />	
		</div>
		
			
		</div>	
	</body>
	<script src = "js/jquery.js"></script>
	<script src = "js/bootstrap.js"></script>
	<script src = "js/jquery.dataTables.js"></script>
	<script type = "text/javascript">
		$(document).ready(function(){
			$('#table').DataTable();
		});
	</script>
	<script type = "text/javascript">
		$(document).ready(function(){
			$('.rstudent_id').click(function(){
				$empID = $(this).attr('name');
				$('.remove_id').click(function(){
					window.location = 'delete_student.php?empID=' + $empID;
				});
			});
			$('.estudent_id').click(function(){
				$empID = $(this).attr('name');
				$('#edit_query').load('load_edit1.php?empID=' + $empID);
			});
		});
	</script>

</html>