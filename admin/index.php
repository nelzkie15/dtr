
<!DOCTYPE html>

<html lang = "eng">
	<head>
		<title>Admin Login</title>
		<meta charset = "utf-8" />
		<meta name = "viewport" content = "width=device-width, initial-scale=1" />
		<link rel = "stylesheet" type = "text/css" href = "css/bootstrap.css" />
		<link rel = "stylesheet" type = "text/css" href = "css/style.css" />

		 <link rel="stylesheet" type="text/css" id="theme" href="cssdesgin/theme-default.css"/>

		<style type="text/css">
			body{
				background-image: url('images/dark.jpg');
			}
				#loader{
		    position: fixed;
		    left: 0px;
		    top: 0px;
		    width: 100%;
		    height: 100%;
		    z-index: 9999;
		    background: url('img/Ajax-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		    opacity: 1;
		}
		input[type="text"],input[type="password"]{
			font-size: 20px;
			
		}
		
		</style>

	</head>
  	<body>

		<nav class = "navbar navbar-inverse navbar-fixed-top">
			<div class = "container-fluid">
				<div class = "navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <p class = ""><?php include('animate/index.html');?></p>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
					
				</div>
			</div>
		</nav>
	</div>
		<div id="loader"></div>

        
        <div class="login-container lightmode">
        
            <div class="login-box animated fadeInDown">
                <div class=""></div>
                <div class="login-body">
                    <div class="login-title"><strong><span class = "glyphicon glyphicon-lock"></span> Admin Security</strong></div>
                    <hr>
                    
                     <form class="form-horizontal" action="login.php" method="POST">
                    <div class="form-group">
                        <div class="col-md-12">

                             <input style="color: #fff2cc" name = "username" type="text" class="form-control" placeholder="Username" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input style="color: #fff2cc" name="password" type="password" class="form-control" placeholder="Password"> 
                        </div>
                    </div> 

                    <div class="form-group">
                        
                        <div class="col-md-12">
                        
                         
                              <input type="submit" name="log" class="btn btn-info btn-block btn-lg"  value="SIGN IN" >  
                        </div>
                    </div>
                    
                    
                    
                    </form>
                </div>
                <div class="login-footer">
                    <div class="">
                    	
                        All right Reserved &copy; <?php echo date('Y');?> Created By:JunilToledo
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </body>
	<script src = "js/jquery.js"></script>
	<script src = "js/bootstrap.js"></script>
	<script src = "js/login.js"></script>
	<script src="jquery.min.js"></script>
	
<script type="text/javascript">
	$(window).on('load', function(){
		//you remove this timeout
		setTimeout(function(){
	        $('#loader').fadeOut('slow');  
	    }, 1000);
	    //remove the timeout
	    //$('#loader').fadeOut('slow'); 
	});
</script>
</html>